# Gosplines Demo
This is a demo application showing how the [Go Splines library][] works.

## Installation

```sh
go install gitlab.com/Achilleshiel/gosplines-demo
```

## Usage
```sh
gosplines-demo
```
Use left-click to add points, remove the last point with right-click.

[Go Splines library]: https://gitlab.com/Achilleshiel/gosplines