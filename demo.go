//go:build cgo

// Package main contains a graphical demo showing the working of the gosplines library.
package main

import (
	"fmt"
	"math"
	"slices"
	"strconv"
	"strings"
	"time"

	"github.com/gonutz/prototype/draw"
	splines "gitlab.com/Achilleshiel/gosplines"
)

const (
	pointSize   = 4
	pointOffset = pointSize / 2
	resolution  = 20
	helpertext  = `
This tool is a demo for cubic splines. The tool will draw lines 
through all the points. Points can be drawn with left-click and 
you can remove the last point with right click.

You can modify the starting direction and strength of the first
and last point. The first point can be modified using WASD, the
last point using the arrow keys. The angles can be reset using 
the 'R' key.
`
)

func main() {
	spline := spline{points: make([]point, 0)}

	err := draw.RunWindow("Title", 1280, 960, spline.update)
	if err != nil {
		panic(err)
	}
}

type point struct {
	X, Y int
}

func (s *spline) floats() (x, y []float64) {
	xs := make([]float64, len(s.points))
	ys := make([]float64, len(s.points))

	for i, point := range s.points {
		xs[i] = float64(point.X)
		ys[i] = float64(point.Y)
	}

	return xs, ys
}

type vector struct {
	angle    float64
	strength float64
}

func (v *vector) inc() {
	v.strength += 10
}

func (v *vector) dec() {
	v.strength -= 10
}

func (v *vector) cw() {
	v.angle--
}

func (v *vector) ccw() {
	v.angle++
}

func (v *vector) x() float64 {
	return v.strength * math.Cos(v.angle/180*math.Pi)
}

func (v *vector) y() float64 {
	return v.strength * math.Sin(v.angle/180*math.Pi)
}

func (v *vector) reset() {
	v.strength = 0
	v.angle = 0
}

type spline struct {
	points     []point
	start, end vector
}

func (s *spline) Add(x, y int) {
	s.points = append(s.points, point{X: x, Y: y})
}

func (s *spline) Calculate(mouseX, mouseY int) ([]point, error) {
	x, y := s.floats()

	xCoefficient, err := splines.SolveSplineWithConstraint(append(x, float64(mouseX)), s.start.x(), -s.end.x())
	if err != nil {
		return nil, fmt.Errorf("unable to calculate coefficients for x: %w", err)
	}

	yCoefficient, err := splines.SolveSplineWithConstraint(append(y, float64(mouseY)), s.start.y(), -s.end.y())
	if err != nil {
		return nil, fmt.Errorf("unable to calculate coefficients for y: %w", err)
	}

	points := make([]point, 0, len(x)*resolution)
	stepSize := 1.0 / float64(resolution-1)

	for i := range len(x) {
		points = append(points, s.points[i])

		for t := stepSize; t < 1.0; t += stepSize {
			xCalculated := xCoefficient[i].Calculate(t)
			yCalculated := yCoefficient[i].Calculate(t)
			points = append(points, point{
				X: int(xCalculated),
				Y: int(yCalculated),
			})
		}
	}

	points = append(points, point{mouseX, mouseY})
	points = slices.Clip(points)

	return points, nil
}

func (s *spline) update(window draw.Window) {
	for _, click := range window.Clicks() {
		if click.Button == 0 {
			s.Add(click.X, click.Y)
		}

		if click.Button == 2 && len(s.points) > 0 {
			s.points = s.points[0 : len(s.points)-1]
		}
	}

	s.handleKeys(window)

	if len(s.points) >= 1 {
		calculateStart := time.Now()
		calculate, err := s.Calculate(window.MousePosition())
		if err != nil {
			window.DrawText(fmt.Sprintf("error calculating: %s", err), 0, 0, draw.White)
			return
		}

		start := calculate[0]
		window.DrawLine(start.X, start.Y, start.X+int(s.start.x()), start.Y+int(s.start.y()), draw.Red)

		end := calculate[len(calculate)-1]
		window.DrawLine(end.X, end.Y, end.X+int(s.end.x()), end.Y+int(s.end.y()), draw.Red)

		for i := range calculate[0 : len(calculate)-1] {
			from := calculate[i]
			to := calculate[i+1]
			window.DrawLine(from.X, from.Y, to.X, to.Y, draw.White)
		}

		duration := strings.Replace(time.Since(calculateStart).String(), "µ", "u", -1)
		window.DrawText(duration, 10, 10, draw.White)
	} else {
		window.DrawText(helpertext, 200, 200, draw.White)
	}

	for i, p := range s.points {
		x, y := p.X-pointOffset, p.Y-pointOffset
		window.FillEllipse(x, y, pointSize, pointSize, draw.White)
		window.DrawText(strconv.Itoa(i)+" ["+strconv.Itoa(x)+","+strconv.Itoa(y)+"]", x+pointSize+2, y+pointSize+5, draw.White)
	}
}

func (s *spline) handleKeys(window draw.Window) {
	if window.IsKeyDown(draw.KeyW) {
		s.start.inc()
	}
	if window.IsKeyDown(draw.KeyS) {
		s.start.dec()
	}
	if window.IsKeyDown(draw.KeyA) {
		s.start.ccw()
	}
	if window.IsKeyDown(draw.KeyD) {
		s.start.cw()
	}
	if window.IsKeyDown(draw.KeyUp) {
		s.end.inc()
	}
	if window.IsKeyDown(draw.KeyDown) {
		s.end.dec()
	}
	if window.IsKeyDown(draw.KeyLeft) {
		s.end.ccw()
	}
	if window.IsKeyDown(draw.KeyRight) {
		s.end.cw()
	}
	if window.IsKeyDown(draw.KeyR) {
		s.end.reset()
		s.start.reset()
	}
	window.DrawText(fmt.Sprintf("start a: %0.2f, s: %0.2f", s.start.angle, s.start.strength), 10, 30, draw.White)
	window.DrawText(fmt.Sprintf("end a: %0.2f, s: %0.2f", s.end.angle, s.end.strength), 10, 50, draw.White)
}
