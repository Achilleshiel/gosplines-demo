module gitlab.com/Achilleshiel/gosplines-demo

go 1.22.1

require (
	github.com/gonutz/prototype v1.2.0
	gitlab.com/Achilleshiel/gosplines v0.0.0-20240602125710-c93b87aea1ee
)

require (
	github.com/gonutz/d3d9 v1.2.1 // indirect
	github.com/gonutz/ds v1.0.0 // indirect
	github.com/gonutz/gl v1.0.0 // indirect
	github.com/gonutz/glfw v1.0.2 // indirect
	github.com/gonutz/go-sdl2 v1.0.0 // indirect
	github.com/gonutz/mixer v1.0.0 // indirect
	github.com/gonutz/w32/v2 v2.2.0 // indirect
	gonum.org/v1/gonum v0.15.0 // indirect
)
